import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native';
import { StackNavigator } from 'react-navigation';

import Loading from './src/screens/Loading';
import Register from './src/screens/Register';
import Login from './src/screens/Login';
import Main from './src/screens/Main';

import NewsInsert from './src/screens/NewsInsert';
import BoardInsert from './src/screens/BoardInsert';
import NewsDetail from './src/screens/NewsDetail';
import BoardDetail from './src/screens/BoardDetail';
import EditProfile from './src/screens/EditProfile';
import OtherProfile from './src/screens/OtherProfile';

import Values from './src/environments/Values';
import Theme from './src/environments/Theme';

const RootStack = StackNavigator({
  Loading: {
    screen: Loading
  },
  Register: {
    screen: Register
  },
  Login: {
    screen: Login
  },
  Main: {
    screen: Main
  },
  NewsInsert: {
    screen: NewsInsert
  },
  BoardInsert: {
    screen: BoardInsert
  },
  NewsDetail: {
    screen: NewsDetail
  },
  BoardDetail: {
    screen: BoardDetail
  },
  EditProfile: {
    screen: EditProfile
  },
  OtherProfile: {
    screen: OtherProfile
  }
}, {
    initialRouteName: 'Loading',
    navigationOptions: {
      headerStyle: {
        backgroundColor: Values.colors.orange
      },
      headerTitleStyle: [{
        fontWeight: 'bold',
      }, Theme.styles.textHeader],
      headerTintColor: Values.colors.white
    }
  })

export default class App extends Component {
  render() {
    return (<RootStack />);
  }
}