import React, { Component } from 'react'
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import Theme from './../environments/Theme';

import FirebaseServices from './../services/FirebaseServices';

export default class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newsData: []
    }
  }
  componentWillMount() {
    FirebaseServices.listenOrderByKey('news', (snap) => {
      var items = [];
      snap.forEach((data) => {
        items.push({
          newsKey: data.key,
          authorId: data
            .val()
            .authorId,
          author: data
            .val()
            .author,
          subject: data
            .val()
            .subject,
          message: data
            .val()
            .message,
          date: data
            .val()
            .date
        });
      });
      this.setState({
        newsData: items.reverse()
      })
    });
  }
  render() {
    let { navigate } = this.props;
    return (
      <View style={Theme.styles.container}>
        <FlatList
          style={Theme.styles.w100}
          keyExtractor={(item, index) => item.newsKey}
          data={this.state.newsData}
          renderItem={({ item }) => <NewsItem item={item} navigate={navigate} />} />
      </View>
    );
  }
}

class NewsItem extends Component {
  render() {
    let { authorId, author, subject, date } = this.props.item;
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => this.props.navigate('NewsDetail', { data: this.props.item })}>

        <View style={Theme.styles.newsItem}>
          <Text style={Theme.styles.textTopic}>{subject}</Text>
          <Text style={Theme.styles.miniText}>{new Date(date).toLocaleString('th-TH', { timeZone: 'Asia/Bangkok' })}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}