import React, {Component} from 'react'
import {
  Alert,
  FlatList,
  TextInput,
  View,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import {
  Button,
  Card,
  Text,
  CardItem,
  Body,
  Left,
  Right
} from 'native-base';
import IconIonicons from 'react-native-vector-icons/Ionicons';

import Theme from './../environments/Theme';
import Strings from './../environments/Strings';
import Values from '../environments/Values';

import FirebaseServices from './../services/FirebaseServices';

export default class BoardDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authorName: '',
      authorStudentId: '',
      commentMessage: '',
      messageErr: false,
      commentName: '',
      commentStudentId: '',
      commentsData: []
    }
    FirebaseServices
      .getCurrentUser()
      .then((data) => this.setState({
        commentName: data.firstname + " " + data.lastname,
        commentStudentId: data.studentId
      }));
    FirebaseServices
      .findUserByUid(this.props.navigation.getParam('data').authorId)
      .then((data) => this.setState({
        authorName: data.firstname + " " + data.lastname,
        authorStudentId: data.studentId
      }));
  }

  componentWillMount() {
    FirebaseServices.listenOrderByKey('comments/' + this.props.navigation.getParam('data').boardKey, (snap) => {
      var items = [];
      snap.forEach((data) => {
        items.push({
          boardKey: this
            .props
            .navigation
            .getParam('data')
            .boardKey,
          commentKey: data.key,
          authorId: data
            .val()
            .authorId,
          commentMessage: data
            .val()
            .commentMessage,
          commentName: data
            .val()
            .commentName,
          commentStudentId: data
            .val()
            .commentStudentId,
          date: data
            .val()
            .date
        });
      });
      this.setState({commentsData: items})
    });
  }

  static navigationOptions = ({navigation}) => {
    return {
      title: <Text style={Theme.styles.textHeader}>{navigation
            .getParam('data')
            .subject}</Text>
    }
  }

  _handlerOnPressPost() {
    let {commentStudentId, commentName, commentMessage} = this.state;

    this.setState({messageErr: false});

    if (commentMessage === '') {
      this.setState({messageErr: true});
      alert(Strings.english.required);
    } else {
      Alert.alert(Strings.english.postConfirmTitle, Strings.english.postConfirmMessage, [
        {
          text: 'No'
        }, {
          text: 'Yes',
          onPress: () => {
            FirebaseServices.comment(this.props.navigation.getParam('data').boardKey, commentStudentId, commentName, commentMessage);
            this.setState({commentMessage: ''})
          }
        }
      ], {cancelable: false})
    }
  }

  render() {
    let {authorId, subject, message, date} = this
      .props
      .navigation
      .getParam('data');

    let {authorName, authorStudentId, commentMessage, messageErr} = this.state
    return (
      <View style={[Theme.styles.container, Theme.styles.p10]}>

        <View style={{
          flexDirection: 'row'
        }}>
          <Card style={Theme.styles.card}>
            <CardItem header>
              <Text style={Theme.styles.h1}>
                {subject}
              </Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={Theme.styles.h4}>
                  {message}
                </Text>
              </Body>
            </CardItem>
            <CardItem footer>
              <Text note>{authorName + ' (' + authorStudentId + ')'}</Text>
              <Right>
                <Text note>{new Date(date).toLocaleString('th-TH', {timeZone: 'Asia/Bangkok'})}</Text>
              </Right>
            </CardItem>
          </Card>
        </View>
        <ScrollView
          ref={ref => this.scrollView = ref}
          onContentSizeChange={(contentWidth, contentHeight) => {
          this
            .scrollView
            .scrollToEnd({animated: true})
        }}>
          <View style={{
            flexDirection: 'row'
          }}>
            <View style={Theme.styles.w100}>
              <FlatList
                style={Theme.styles.w100}
                keyExtractor={(item, index) => item.commentKey}
                data={this.state.commentsData}
                renderItem={({item}) => <BoardDetailItem navigate={this.props.navigation.navigate} item={item}/>}/>
              <Card style={Theme.styles.card}>
                <TextInput
                  style={[messageErr
                    ? Theme.styles.inputErr
                    : Theme.styles.input]}
                  multiline={true}
                  height={100}
                  placeholder={Strings.english.comment}
                  onChangeText={(commentMessage) => this.setState({commentMessage})}
                  value={commentMessage}/>
                <Button full warning onPress={() => this._handlerOnPressPost()}>
                  <Text>{Strings.english.post}</Text>
                </Button>
              </Card>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

class BoardDetailItem extends Component {

  _handlerOnPressName() {
    this
      .props
      .navigate('OtherProfile', {userId: this.props.item.authorId})
  }

  renderCommentName = (commentName, authorId, commentStudentId) => {
    if (!FirebaseServices.isCurrentUserById(authorId)) {
      return (
        <TouchableOpacity onPress={() => this._handlerOnPressName()}>
          <Text
            style={[
            Theme.styles.textTopic, {
              color: Values.colors.linkBlue
            }
          ]}>{commentName}
            ({commentStudentId})</Text>
        </TouchableOpacity>
      )
    } else {
      return (
        <Text style={Theme.styles.textTopic}>{commentName}
          ({commentStudentId})</Text>
      )
    }
  }

  renderDelButton = (authorId, boardKey, commentKey) => {
    if (FirebaseServices.isCurrentUserById(authorId)) {
      return <IconIonicons
        size={24}
        name='ios-close'
        onPress={() => {
        Alert.alert(Strings.english.delConfirmTitle, Strings.english.delConfirmMessage, [
          {
            text: 'No'
          }, {
            text: 'Yes',
            onPress: () => {
              FirebaseServices.remove('comments/' + boardKey + '/' + commentKey)
            }
          }
        ], {cancelable: false})
      }}/>
    }
  }

  render() {
    let {
      boardKey,
      commentKey,
      authorId,
      commentStudentId,
      commentName,
      commentMessage,
      date
    } = this.props.item;
    return (
      <Card style={Theme.styles.newsItem}>
        <CardItem
          header
          style={{
          flexDirection: 'row',
          justifyContent: 'space-between'
        }}>
          {this.renderCommentName(commentName, authorId, commentStudentId)}
          {this.renderDelButton(authorId, boardKey, commentKey)}
        </CardItem>
        <CardItem>
          <Body>
            <Text style={Theme.styles.h4}>{commentMessage}</Text>
          </Body>
        </CardItem>
        <CardItem footer>
          <Text note>{new Date(date).toLocaleString('th-TH', {timeZone: 'Asia/Bangkok'})}</Text>
        </CardItem>
      </Card>
    );
  }
}