import React, {Component} from 'react'
import {Alert, TextInput, StyleSheet, View} from 'react-native';
import {Button, Text} from 'native-base';

import Theme from './../environments/Theme';
import Strings from './../environments/Strings';

import FirebaseServices from './../services/FirebaseServices';

export default class NewsInsert extends Component {

  constructor(props) {
    super(props);
    this.state = {
      author: '',
      subject: '',
      message: '',
      authorErr: false,
      subjectErr: false,
      messageErr: false
    }
  }

  static navigationOptions = {
    title: <Text style={Theme.styles.textHeader}>{Strings.english.newsInsertHeader}</Text>
  }

  _handlerOnPressPost() {
    let {author, subject, message} = this.state;
    let pass = true;

    this.setState({authorErr: false, subjectErr: false, messageErr: false});

    if (author === '') {
      pass = false;
      this.setState({authorErr: true});
    }
    if (subject === '') {
      pass = false;
      this.setState({subjectErr: true});
    }
    if (message === '') {
      pass = false;
      this.setState({messageErr: true});
    }
    if (pass) {
      Alert.alert(Strings.english.postConfirmTitle, Strings.english.postConfirmMessage, [
        {
          text: 'No'
        }, {
          text: 'Yes',
          onPress: () => {
            FirebaseServices.post("news", author, subject, message);
            this
              .props
              .navigation
              .navigate('Main');
          }
        }
      ], {cancelable: false})
    } else {
      alert(Strings.english.required);
    }
  }

  render() {
    let {
      author,
      subject,
      message,
      authorErr,
      subjectErr,
      messageErr
    } = this.state;
    return (
      <View style={Theme.styles.container}>
        <View style={Theme.styles.p10}>
          <TextInput
            style={authorErr
            ? Theme.styles.inputErr
            : Theme.styles.input}
            placeholder={Strings.english.author}
            onChangeText={(author) => this.setState({author})}/>
          <TextInput
            style={subjectErr
            ? Theme.styles.inputErr
            : Theme.styles.input}
            placeholder={Strings.english.subject}
            onChangeText={(subject) => this.setState({subject})}/>
          <TextInput
            style={messageErr
            ? Theme.styles.inputErr
            : Theme.styles.input}
            multiline={true}
            height={200}
            placeholder={Strings.english.message}
            onChangeText={(message) => this.setState({message})}/>
          <Button full warning onPress={() => this._handlerOnPressPost()}>
            <Text>{Strings.english.post}</Text>
          </Button>
        </View>
      </View>
    );
  }
}