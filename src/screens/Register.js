import React, {Component} from 'react'
import {StyleSheet, View, ScrollView} from 'react-native';
import {Button, Text} from 'native-base';
import t from 'tcomb-form-native';

import FirebaseServices from '../services/FirebaseServices';

import Theme from './../environments/Theme';
import Strings from '../environments/Strings';
import Values from '../environments/Values';

const Form = t.form.Form;

const User = t.struct({
  email: t.String,
  password: t.String,
  studentId: t.Number,
  firstname: t.String,
  lastname: t.String,
  faculty: t.String,
  branch: t.String,
  mobile: t.String
});

const formStyles = {
  ...Form.stylesheet,
  formGroup: {
    normal: {
      marginBottom: 10
    }
  },
  controlLabel: {
    normal: Theme.styles.form,
    error: Theme.styles.error
  }
}

export default class Register extends Component {

  static navigationOptions = {
    title: 'Register',
    headerTitleStyle: Theme.styles.textHeader
  }

  _signup = () => {
    const value = this
      .refs
      ._form
      .getValue()
    console.log('value', value)
    console.log('saved')
    if (value) {
      FirebaseServices.register(this.props.navigation, value.email, value.password, value);
    }
  }

  render() {
    let options = {
      fields: {
        email: {
          label: 'Email:',
          autoCapitalize: 'none',
          error: Strings.english.emailErr
        },
        password: {
          label: 'Password:',
          secureTextEntry: true,
          maxLength: 12,
          error: Strings.english.passwordErr
        },
        firstname: {
          label: 'First name:',
          error: Strings.english.fnameErr
        },
        lastname: {
          label: 'Last name:'
        },
        studentId: {
          label: 'Student ID',
          maxLength: 11,
          error: Strings.english.stdIdErr
        },
        faculty: {
          label: 'Faculty:'
        },
        branch: {
          label: 'Branch:'
        },
        mobile: {
          label: 'Mobile:',
          maxLength: 10
        }
      },
      stylesheet: formStyles
    }
    return (
      <View style={Theme.styles.container}>
        <ScrollView style={Theme.styles.w100}>
          <View style={Theme.styles.p10}>
            <Form ref='_form' type={User} options={options}/>
            <Button full warning onPress={this._signup}>
              <Text>Sign up</Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    );
  }
}