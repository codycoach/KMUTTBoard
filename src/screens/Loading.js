import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native';
import { Spinner } from 'native-base';
import FirebaseServices from './../services/FirebaseServices';
import Values from './../environments/Values';
import Theme from './../environments/Theme';
export default class Loading extends Component {

  componentDidMount() {
    FirebaseServices.checkAuth(this.props.navigation);
  }

  static navigationOptions = {
    header: null
  }
  
  render() {
    return (
      <View style={styles.container}>
        <Spinner color={Values.colors.orange} />
        <Text>Loading...</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  }
});