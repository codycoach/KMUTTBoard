import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native';
import { Card, CardItem, Text, Body, Right } from 'native-base';

import Theme from './../environments/Theme';
export default class NewsDetail extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: <Text style={Theme.styles.textHeader}>{navigation
        .getParam('data')
        .subject}</Text>
    }
  }

  render() {
    let { authorId, author, subject, message, date } = this
      .props
      .navigation
      .getParam('data')
    return (
      <View style={[Theme.styles.container, Theme.styles.p10]}>
        <View style={{
          flexDirection: 'row'
        }}>
          <Card style={Theme.styles.card}>
            <CardItem header>
              <Text style={Theme.styles.h1}>
                {subject}
              </Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={Theme.styles.h4}>
                  {message}
                </Text>
              </Body>
            </CardItem>
            <CardItem footer>
              <Text note>Creator: {author}</Text>
              <Right>
                <Text note>{new Date(date).toLocaleString('th-TH', { timeZone: 'Asia/Bangkok' })}</Text>
              </Right>
            </CardItem>
          </Card>
        </View>
      </View>
    );
  }
}