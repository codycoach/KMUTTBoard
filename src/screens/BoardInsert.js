import React, {Component} from 'react'
import {Alert, StyleSheet, TextInput, View} from 'react-native';
import {Button, Text} from 'native-base';

import Theme from './../environments/Theme';
import Strings from './../environments/Strings';

import FirebaseServices from './../services/FirebaseServices';

export default class BoardInsert extends Component {

    constructor(props) {
        super(props);
        this.state = {
            authorId: '',
            subject: '',
            message: '',
            subjectErr: false,
            messageErr: false
        }
    }

    static navigationOptions = {
        title: <Text style={Theme.styles.textHeader}>{Strings.english.boardInsertHeader}</Text>
    };

    _handlerOnPressPost() {
        let {subject, message} = this.state;
        let pass = true;

        this.setState({subjectErr: false, messageErr: false});

        if (subject === '') {
            pass = false;
            this.setState({subjectErr: true});
        }
        if (message === '') {
            pass = false;
            this.setState({messageErr: true});
        }
        if (pass) {
            Alert.alert(Strings.english.postConfirmTitle, Strings.english.postConfirmMessage, [
                {
                    text: 'No'
                }, {
                    text: 'Yes',
                    onPress: () => {
                        FirebaseServices.post("board", null, subject, message);
                        this
                            .props
                            .navigation
                            .navigate('Main');
                    }
                }
            ], {cancelable: false})
        } else {
            alert(Strings.english.required);
        }
    }

    render() {
        let {subject, message, subjectErr, messageErr} = this.state;
        return (
            <View style={Theme.styles.container}>
                <View style={Theme.styles.p10}>
                    <TextInput
                        style={subjectErr
                        ? Theme.styles.inputErr
                        : Theme.styles.input}
                        placeholder={Strings.english.subject}
                        onChangeText={(subject) => this.setState({subject})}/>
                    <TextInput
                        style={messageErr
                        ? Theme.styles.inputErr
                        : Theme.styles.input}
                        multiline={true}
                        height={200}
                        placeholder={Strings.english.message}
                        onChangeText={(message) => this.setState({message})}/>
                    <Button full warning onPress={() => this._handlerOnPressPost()}>
                        <Text>{Strings.english.post}</Text>
                    </Button>
                </View>
            </View>
        );
    }
}