import {StyleSheet} from 'react-native';
import Values from './Values';
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: Values.colors.white
    },
    containerLogin: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Values.colors.orange
    },
    headerLogin: {
        fontSize: 60,
        fontWeight: '600',
        color: Values.colors.white,
        textAlign: 'center',
        marginBottom: 20
    },
    inputLogin: {
        backgroundColor: 'white',
        paddingLeft: 15,
        marginLeft: 30,
        marginRight: 30,
        marginBottom: 10
    },
    buttonLogin: {
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 20
    },
    textRegister: {
        color: Values.colors.linkBlue,
        marginLeft: 5
    },
    textHeader: {
        marginHorizontal: 10,
        color: Values.colors.white,
        fontSize: Values.fontSize.textHeader,
        fontWeight: 'normal'
    },
    input: {
        fontSize: 20,
        borderWidth: 1,
        borderColor: Values.colors.silver,
        padding: 10,
        marginBottom: 10
    },
    inputErr: {
        fontSize: 20,
        borderWidth: 1,
        borderColor: Values.colors.red,
        padding: 10,
        marginBottom: 10
    },
    m10: {
        marginLeft: 10,
        marginTop: 10,
        marginRight: 10,
        marginBottom: 10
    },
    p10: {
        width: '100%',
        padding: 10
    },
    flexRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 20
    },
    row: {
        flex: 1,
        flexDirection: 'row'
    },
    newsItem: {
        padding: 15,
        borderBottomWidth: 0.8,
        borderBottomColor: Values.colors.silver
    },
    textTopic: {
        fontSize: 20,
        marginBottom: 10,
        fontWeight: "500",
        color: Values.colors.black
    },
    miniText: {
        color: Values.colors.grey
    },
    textDetail: {
        fontSize: 18,
        padding: 10
    },
    w100: {
        width: '100%'
    },
    h1: {
        fontSize: Values.fontSize.h1,
        fontWeight: '500',
        color: Values.colors.black
    },
    h4: {
        fontSize: Values.fontSize.h4,
        fontWeight: '200',
        color: Values.colors.black
    },
    h5: {
        fontSize: Values.fontSize.h5,
        fontWeight: '100',
        color: Values.colors.black
    },
    card: {
        width: '100%',
        padding: 15
    },
    error: {
        color: Values.colors.red,
        fontSize: 16,
        marginBottom: 3,
        fontWeight: 'normal'
    },
    form: {
        fontSize: 16,
        borderColor: Values.colors.silver,
        padding: 10
    },
    logoutButton: {
        paddingHorizontal: 30,
        paddingVertical: 5
    },
    flex2label: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileRow: {
        flexDirection: 'row',
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: Values.colors.silver
    },
    profileLabel: {
        flex: 1,
        fontSize: Values.fontSize.h3
    },
    profileValue: {
        flex: 1,
        fontSize: Values.fontSize.h3,
        fontWeight: '200'
    }
});
export default {
    styles
};