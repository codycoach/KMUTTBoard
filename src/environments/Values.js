let colors = {
    black: "#222021",
    blue: "lightskyblue",
    green: "palegreen",
    linkBlue: "#2F84EA",
    orange: "#e67e22",
    pink: "pink",
    red: "tomato",
    silver: "#E4E4E4",
    violet: "plum",
    white: "#F5FCFF",
    yellow: "khaki",
    grey: '#626262'
}

let numbers = {
    currentYear: 2561
}

let fontSize = {
    textHeader: 26,
    icon: 26,
    h1: 26,
    h2: 24,
    h3: 22,
    h4: 20,
    h5: 18
}

export default {
    colors,
    numbers,
    fontSize
}