let english = {
    author: 'Penname',
    board: 'Board',
    boardInsertHeader: 'Add new topic',
    chat: 'Chat',
    comment: 'Comment',
    delConfirmMessage: 'Delete this post.',
    delConfirmTitle: 'Are you sure ?',
    emailErr: 'Email is required!',
    fnameErr: 'Firstname is required!',
    goLogout: 'Do you want to Log out?',
    loginErr: 'Please enter email/password.',
    logout: 'Logout',
    message: 'Message',
    news: 'News',
    newsInsertHeader: 'Add new news',
    passwordErr: 'Password is required!',
    post: 'Post',
    postConfirmMessage: 'You cannot edit after confirmed.',
    postConfirmTitle: 'Are you sure ?',
    profile: 'Profile',
    registerComp: 'Registeration complete',
    required: 'All fields are required',
    stdIdErr: 'Student ID must be numbers',
    subject: 'Subject',
}

export default {
    english
}